module coopersql

go 1.19

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/julienschmidt/httprouter v1.3.0
)
