package models

import (
	"database/sql"
	"errors"
	"fmt"
)

type Task struct {
	ID       int    `json:"taskID,omitempty"`
	Project  int    `json:"taskProject,omitempty"`
	Employee int    `json:"taskEmployee,omitempty"`
	Status   string `json:"taskStatus,omitempty"`
}

type TaskModel struct {
	DB *sql.DB
}

func (m *TaskModel) Get(id int) (*Task, error) {
	stmt := `
	SELECT taskID, taskProject, taskEmployee, taskStatus
	WHERE taskID = ?
	FROM oraclepm.tasks;
	`

	row := m.DB.QueryRow(stmt, id)
	task := &Task{}

	err := row.Scan(
		&task.ID,
		&task.Project,
		&task.Employee,
		&task.Status,
	)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, ErrNoRecord
		} else {
			return nil, err
		}
	}

	return task, nil
}

func (m *TaskModel) GetAll() (*[]Task, error) {
	stmt := `
	SELECT taskID, taskProject, taskEmployee, taskStatus
	FROM oraclepm.tasks;
	`

	rows, err := m.DB.Query(stmt)
	var tasks []Task
	task := Task{}

	for i := 0; rows.Next(); i++ {
		err := rows.Scan(
			&task.ID,
			&task.Project,
			&task.Employee,
			&task.Status,
		)
		if err != nil {
			break
		}
		tasks = append(tasks, task)
		fmt.Printf("%+v", task)
	}

	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, ErrNoRecord
		} else {
			return nil, err
		}
	}

	return &tasks, nil
}

func (m *TaskModel) Insert(task Task) (int, error) {
	stmt := `
	INSERT INTO oraclepm.tasks
	(taskProject, taskEmployee, taskStatus)
	VALUES(0, '@taskProgress', @taskEmployee, '@taskPriority');
	`

	result, err := m.DB.Exec(
		stmt,
		task.ID,
		task.Project,
		task.Employee,
		task.Status,
	)
	if err != nil {
		return 0, err
	}

	id, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}

	return int(id), nil
}
