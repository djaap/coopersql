package main

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
)

func (app *application) routes() http.Handler {
	router := httprouter.New()
	router.HandlerFunc(http.MethodGet, "/user/get/:id", app.GetUser)
	router.HandlerFunc(http.MethodGet, "/user/login/:username/:password", app.UserLogin)
	router.HandlerFunc(http.MethodGet, "/user/getall", app.GetAllUsers)
	router.HandlerFunc(http.MethodPost, "/user/create", app.CreateUser)

	router.HandlerFunc(http.MethodGet, "/project/get/:id", app.GetProject)
	router.HandlerFunc(http.MethodGet, "/project/getall", app.GetAllProjects)
	router.HandlerFunc(http.MethodPost, "/project/create", app.CreateProject)

	router.HandlerFunc(http.MethodGet, "/task/get/:id", app.GetTask)
	router.HandlerFunc(http.MethodGet, "/task/getall", app.GetAllTasks)
	router.HandlerFunc(http.MethodPost, "/task/create", app.CreateTask)
	return router
}
