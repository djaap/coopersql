package main

import (
	"coopersql/internal/models"
	"database/sql"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	_ "github.com/go-sql-driver/mysql"
)

type application struct {
	user     *models.UserModel
	project  *models.ProjectModel
	task     *models.TaskModel
	infoLog  *log.Logger
	errorLog *log.Logger
}

func main() {
	addr := flag.String("addr", ":4001", "HTTP network address")
	usr := flag.String("user", "web", "database user")
	pass := flag.String("pass", "pass", "database password")
	dbName := flag.String("dbname", "oraclepm", "database name")
	flag.Parse()

	infoLog := log.New(os.Stdout, "INFO", log.Ltime|log.Lshortfile)
	errorLog := log.New(os.Stderr, "INFO", log.Ltime|log.Lshortfile)

	dsn := fmt.Sprintf("%s:%s@/%s?parseTime=true", *usr, *pass, *dbName)
	db, err := openDB(dsn)
	if err != nil {
		errorLog.Fatal(err.Error())
		return
	}
	defer db.Close()

	app := &application{
		user:     &models.UserModel{DB: db},
		project:  &models.ProjectModel{DB: db},
		task:     &models.TaskModel{DB: db},
		infoLog:  infoLog,
		errorLog: errorLog,
	}

	app.infoLog.Printf("Stating server on %s", *addr)
	http.ListenAndServe(*addr, app.routes())
}

func openDB(dsn string) (*sql.DB, error) {
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		return nil, err
	}
	if err = db.Ping(); err != nil {
		return nil, err
	}
	return db, nil
}
