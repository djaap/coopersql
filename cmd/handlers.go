package main

import (
	"coopersql/internal/models"
	"encoding/json"
	"errors"
	"net/http"
	"strconv"

	"github.com/julienschmidt/httprouter"
)

func (app *application) GetUser(w http.ResponseWriter, r *http.Request) {
	params := httprouter.ParamsFromContext(r.Context())

	id, err := strconv.Atoi(params.ByName("id"))
	if err != nil || id < 1 {
		app.notFound(w)
		return
	}

	user, err := app.user.Get(id)
	if err != nil {
		if errors.Is(err, models.ErrNoRecord) {
			app.notFound(w)
		} else {
			app.serverError(w, err)
		}
	}

	userBytes, err := json.Marshal(user)
	if err != nil {
		app.errorLog.Println(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(userBytes)
}

func (app *application) UserLogin(w http.ResponseWriter, r *http.Request) {
	params := httprouter.ParamsFromContext(r.Context())

	username := params.ByName("username")
	password := params.ByName("password")

	user, err := app.user.Login(username, password)
	if err != nil {
		if errors.Is(err, models.ErrNoRecord) {
			app.notFound(w)
		} else {
			app.serverError(w, err)
		}
	}

	userBytes, err := json.Marshal(user)
	if err != nil {
		app.errorLog.Println(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(userBytes)
}

func (app *application) GetAllUsers(w http.ResponseWriter, r *http.Request) {

	users, err := app.user.GetAll()
	if err != nil {
		if errors.Is(err, models.ErrNoRecord) {
			app.notFound(w)
		} else {
			app.serverError(w, err)
		}
	}

	userBytes, err := json.Marshal(users)
	if err != nil {
		app.errorLog.Println(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(userBytes)
}

func (app *application) CreateUser(w http.ResponseWriter, r *http.Request) {
	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()

	var user models.User
	if err := dec.Decode(&user); err != nil {
		app.clientError(w, http.StatusBadRequest)
	}

	id, err := app.user.Insert(user)
	if err != nil {
		app.serverError(w, err)
		return
	}
	idStruct := &struct {
		UserID int `json:"userID"`
	}{UserID: id}
	idBytes, err := json.Marshal(idStruct)
	if err != nil {
		app.serverError(w, err)
		return
	}
	app.infoLog.Println(string(idBytes))

	w.WriteHeader(http.StatusOK)
	w.Write(idBytes)
}

func (app *application) GetProject(w http.ResponseWriter, r *http.Request) {
	params := httprouter.ParamsFromContext(r.Context())

	id, err := strconv.Atoi(params.ByName("id"))
	if err != nil || id < 1 {
		app.notFound(w)
		return
	}

	project, err := app.project.Get(id)
	if err != nil {
		if errors.Is(err, models.ErrNoRecord) {
			app.notFound(w)
		} else {
			app.serverError(w, err)
		}
	}

	projectBytes, err := json.Marshal(project)
	if err != nil {
		app.errorLog.Println(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(projectBytes)
}

func (app *application) GetAllProjects(w http.ResponseWriter, r *http.Request) {

	projects, err := app.project.GetAll()
	if err != nil {
		if errors.Is(err, models.ErrNoRecord) {
			app.notFound(w)
		} else {
			app.serverError(w, err)
		}
	}

	projectBytes, err := json.Marshal(projects)
	if err != nil {
		app.errorLog.Println(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(projectBytes)
}

func (app *application) CreateProject(w http.ResponseWriter, r *http.Request) {
	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()

	var project models.Project
	if err := dec.Decode(&project); err != nil {
		app.clientError(w, http.StatusBadRequest)
	}

	id, err := app.project.Insert(project)
	if err != nil {
		app.serverError(w, err)
		return
	}
	idStruct := &struct {
		ProjectID int `json:"projectID"`
	}{ProjectID: id}
	idBytes, err := json.Marshal(idStruct)
	if err != nil {
		app.serverError(w, err)
		return
	}
	app.infoLog.Println(string(idBytes))

	w.WriteHeader(http.StatusOK)
	w.Write(idBytes)
}

func (app *application) GetTask(w http.ResponseWriter, r *http.Request) {
	params := httprouter.ParamsFromContext(r.Context())

	id, err := strconv.Atoi(params.ByName("id"))
	if err != nil || id < 1 {
		app.notFound(w)
		return
	}

	task, err := app.task.Get(id)
	if err != nil {
		if errors.Is(err, models.ErrNoRecord) {
			app.notFound(w)
		} else {
			app.serverError(w, err)
		}
	}

	taskBytes, err := json.Marshal(task)
	if err != nil {
		app.errorLog.Println(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(taskBytes)
}

func (app *application) GetAllTasks(w http.ResponseWriter, r *http.Request) {

	tasks, err := app.task.GetAll()
	if err != nil {
		if errors.Is(err, models.ErrNoRecord) {
			app.notFound(w)
		} else {
			app.serverError(w, err)
		}
	}

	taskBytes, err := json.Marshal(tasks)
	if err != nil {
		app.errorLog.Println(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(taskBytes)
}

func (app *application) CreateTask(w http.ResponseWriter, r *http.Request) {
	dec := json.NewDecoder(r.Body)
	dec.DisallowUnknownFields()

	var task models.Task
	if err := dec.Decode(&task); err != nil {
		app.clientError(w, http.StatusBadRequest)
	}

	id, err := app.task.Insert(task)
	if err != nil {
		app.serverError(w, err)
		return
	}
	idStruct := &struct {
		TaskID int `json:"taskID"`
	}{TaskID: id}
	idBytes, err := json.Marshal(idStruct)
	if err != nil {
		app.serverError(w, err)
		return
	}
	app.infoLog.Println(string(idBytes))

	w.WriteHeader(http.StatusOK)
	w.Write(idBytes)
}
