# How to Use
```sh
	go run ./cmd/... -user="username" -pass="password" -dbname="oraclepm"
```

# requests
* curl localhost:4001/user/get/1
* curl -X POST localhost:4001/user/create -H 'Content-Type: application/json' -d '{"userName":"CJaap","lastName":"Jaap","firstName":"Cooper","phoneNumber":"1231231234","uEmail":"cjaap@uccs.edu","access":"Admin"}' 
